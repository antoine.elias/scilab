# Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
# Copyright (C) Dassault Systemes - 2022 - Clement DAVID
#
#
# This YAML file describe the test stage of the CI. Tests run are defined
# per platform and split in good, bad and ugly sets. Special rules named
# impacted are added for modules modified by Merge Request.
#

.windows_test:
  stage: test
  tags: [x64_windows, scilab]
  script:
    - cmd /C "call .gitlab-ci\test.bat"
  artifacts:
    when: always
    paths:
      - "${SCI_VERSION_STRING}/${TEST}.xml"
    reports:
      junit: "${SCI_VERSION_STRING}/${TEST}.xml"

windows_good_mr:
  extends: .windows_test
  needs:
    - windows_build_mr
    - windows_set_env
  rules: &MR_RULES
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event' || $CI_PIPELINE_SOURCE == 'push'
  when: manual
  parallel:
    matrix: &GOOD_TEST_ANCHOR
      - TEST:
          - api_scilab
          - arnoldi
          - ast
          - boolean
          - completion
          - core
          - data_structures
          - fftw
          - fileio
          - functions
          - history_manager
          - integer
          - parameters
          - polynomials
          - special_functions
          - spreadsheet
          - string
          - webtools
          - xml

windows_bad_mr:
  extends: .windows_test
  rules: *MR_RULES
  when: manual
  parallel:
    matrix: &BAD_TEST_ANCHOR
      - TEST:
          - cacsd
          - console
          - coverage
          - demo_tools
          - development_tools
          - differential_equations
          - dynamic_link
          - elementary_functions
          - external_objects_java
          - graphic_export
          - graphic_objects
          - graphics
          - gui
          - hdf5
          - helptools
          - interpolation
          - io
          - jvm
          - linear_algebra
          - localization
          - m2sci
          - matio
          - mexlib
          - modules_manager
          - optimization
          - output_stream
          - overloading
          - randlib
          - renderer
          - scicos
          - scicos_blocks
          - scinotes
          - signal_processing
          - slint
          - sound
          - sparse
          - statistics
          - tclsci
          - time
          - ui_data
          - umfpack

windows_ugly_mr:
  extends: .windows_test
  rules: *MR_RULES
  when: manual
  parallel:
    matrix: &UGLY_TEST_ANCHOR
      - TEST:
          - atoms
          - xcos

windows_only_mr:
  extends: .windows_test
  rules: *MR_RULES
  when: manual
  parallel:
    matrix: &WINDOWS_ONLY_TEST_ANCHOR
      - TEST:
          - mpi
          - windows_tools

windows_good_nightly:
  extends: .windows_test
  needs:
    - windows_build_nightly
    - windows_set_env
  rules:
    - if: $CI_PIPELINE_SOURCE == 'schedule'
  parallel:
    matrix: *GOOD_TEST_ANCHOR

windows_bad_nightly:
  extends: .windows_test
  needs:
    - windows_build_nightly
    - windows_set_env
  rules:
    - if: $CI_PIPELINE_SOURCE == 'schedule'
  parallel:
    matrix: *BAD_TEST_ANCHOR

windows_ugly_nightly:
  extends: .windows_test
  needs:
    - windows_build_nightly
    - windows_set_env
  rules:
    - if: $CI_PIPELINE_SOURCE == 'schedule'
  parallel:
    matrix: *UGLY_TEST_ANCHOR

windows_only_nightly:
  extends: .windows_test
  needs:
    - windows_build_nightly
    - windows_set_env
  rules:
    - if: $CI_PIPELINE_SOURCE == 'schedule'
  parallel:
    matrix: *WINDOWS_ONLY_TEST_ANCHOR

windows_good_release:
  extends: .windows_test
  needs:
    - windows_build_release
    - windows_set_env
  rules:
    - if: $CI_COMMIT_TAG
  parallel:
    matrix: *GOOD_TEST_ANCHOR

windows_bad_release:
  extends: .windows_test
  needs:
    - windows_build_release
    - windows_set_env
  rules:
    - if: $CI_COMMIT_TAG
  parallel:
    matrix: *BAD_TEST_ANCHOR

windows_ugly_release:
  extends: .windows_test
  needs:
    - windows_build_release
    - windows_set_env
  rules:
    - if: $CI_COMMIT_TAG
  parallel:
    matrix: *UGLY_TEST_ANCHOR

windows_only_release:
  extends: .windows_test
  needs:
    - windows_build_release
    - windows_set_env
  rules:
    - if: $CI_COMMIT_TAG
  parallel:
    matrix: *WINDOWS_ONLY_TEST_ANCHOR

windows_good_impacted:
  extends: .windows_test
  needs:
    - windows_build_mr
    - windows_set_env
  rules: &IMPACTED_RULES
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event' || $CI_PIPELINE_SOURCE == 'push'
      changes:
        - scilab/modules/$TEST/**/*
  parallel:
    matrix: *GOOD_TEST_ANCHOR

windows_bad_impacted:
  extends: .windows_test
  needs:
    - windows_build_mr
    - windows_set_env
  rules: *IMPACTED_RULES
  parallel:
    matrix: *BAD_TEST_ANCHOR

windows_ugly_impacted:
  extends: .windows_test
  needs:
    - windows_build_mr
    - windows_set_env
  rules: *IMPACTED_RULES
  parallel:
    matrix: *UGLY_TEST_ANCHOR

windows_only_impacted:
  extends: .windows_test
  needs:
    - windows_build_mr
    - windows_set_env
  rules: *IMPACTED_RULES
  parallel:
    matrix: *WINDOWS_ONLY_TEST_ANCHOR

.linux_test:
  stage: test
  tags: [x86_64-linux-gnu, docker, scilab]
  image: $DOCKER_LINUX_BUILDER
  script: bash -e -x .gitlab-ci/test.sh
  artifacts:
    when: always
    paths:
      - "${SCI_VERSION_STRING}/${TEST}.xml"
    reports:
      junit: "${SCI_VERSION_STRING}/${TEST}.xml"

linux_good_mr:
  extends: .linux_test
  needs:
    - linux_build_mr
    - linux_set_env
  rules: *MR_RULES
  when: manual
  parallel:
    matrix: *GOOD_TEST_ANCHOR

linux_bad_mr:
  extends: .linux_test
  needs:
    - linux_build_mr
    - linux_set_env
  rules: *MR_RULES
  when: manual
  parallel:
    matrix: *BAD_TEST_ANCHOR

linux_ugly_mr:
  extends: .linux_test
  needs:
    - linux_build_mr
    - linux_set_env
  rules: *MR_RULES
  when: manual
  parallel:
    matrix: *UGLY_TEST_ANCHOR

linux_good_nightly:
  extends: .linux_test
  needs:
    - linux_build_nightly
    - linux_set_env
  rules:
    - if: $CI_PIPELINE_SOURCE == 'schedule'
  parallel:
    matrix: *GOOD_TEST_ANCHOR

linux_bad_nightly:
  extends: .linux_test
  needs:
    - linux_build_nightly
    - linux_set_env
  rules:
    - if: $CI_PIPELINE_SOURCE == 'schedule'
  parallel:
    matrix: *BAD_TEST_ANCHOR

linux_ugly_nightly:
  extends: .linux_test
  needs:
    - linux_build_nightly
    - linux_set_env
  rules:
    - if: $CI_PIPELINE_SOURCE == 'schedule'
  parallel:
    matrix: *UGLY_TEST_ANCHOR

linux_good_release:
  extends: .linux_test
  needs:
    - linux_build_release
    - linux_set_env
  rules:
    - if: $CI_COMMIT_TAG
  parallel:
    matrix: *GOOD_TEST_ANCHOR

linux_bad_release:
  extends: .linux_test
  needs:
    - linux_build_release
    - linux_set_env
  rules:
    - if: $CI_COMMIT_TAG
  parallel:
    matrix: *BAD_TEST_ANCHOR

linux_ugly_release:
  extends: .linux_test
  needs:
    - linux_build_release
    - linux_set_env
  rules:
    - if: $CI_COMMIT_TAG
  parallel:
    matrix: *UGLY_TEST_ANCHOR

linux_good_impacted:
  extends: .linux_test
  needs:
    - linux_build_mr
    - linux_set_env
  rules: *IMPACTED_RULES
  parallel:
    matrix: *GOOD_TEST_ANCHOR

linux_bad_impacted:
  extends: .linux_test
  needs:
    - linux_build_mr
    - linux_set_env
  rules: *IMPACTED_RULES
  parallel:
    matrix: *BAD_TEST_ANCHOR

linux_ugly_impacted:
  extends: .linux_test
  needs:
    - linux_build_mr
    - linux_set_env
  rules: *IMPACTED_RULES
  parallel:
    matrix: *UGLY_TEST_ANCHOR
